/*  Author: Jeff Y Luo
    For ECE 155 - Spring 2015
    University of Waterloo
    Department of Electrical and Computer Engineering */

package ca.uwaterloo.jeff.gyroball;

import ca.uwaterloo.jeff.gyroball.R;
import ca.uwaterloo.jeff.gyroball.Sensors.GyroscopeEventListener;
import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity{
	private MainActivity mainActivity = this;
	private SensorManager sensorManager;
	private Sensor gyroscope;
	private SensorEventListener gyroListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// New Game Button
		Button newGameBtn = (Button) this.findViewById(R.id.main_new_game_button);
		newGameBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(mainActivity, GameActivity.class);
				startActivity(i);
			}
		});

		// Gyro Reset Button
		Button resetGyroBtn = (Button) this.findViewById(R.id.main_reset_gyro_button);
		resetGyroBtn.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				((GyroscopeEventListener) gyroListener).reset();
			}
		});

		// Setup the Gyroscope Sensor
		sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
		gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		initalizeGyroSensor();
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d("Brick", "app paused");
	}

	@Override
	protected void onResume() {
		initalizeGyroSensor();
		super.onResume();
		Log.d("Brick", "app resumed");
	}

	private void initalizeGyroSensor()
	{
		TextView gyro_x = (TextView) this.findViewById(R.id.main_gyroscope_textview_x);
		TextView gyro_y = (TextView) this.findViewById(R.id.main_gyroscope_textview_y);
		TextView gyro_z = (TextView) this.findViewById(R.id.main_gyroscope_textview_z);

		GyroscopeEventListener.removeListener();
		gyroListener = GyroscopeEventListener.getListener(gyro_x, gyro_y, gyro_z);
		sensorManager.registerListener(gyroListener, gyroscope, SensorManager.SENSOR_DELAY_GAME);
	}
}
