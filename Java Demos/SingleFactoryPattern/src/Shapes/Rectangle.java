package Shapes;

import Program.Shape;

//a concrete product of Shape
public class Rectangle implements Shape{

	private double length;
	private double width;
	private String name;
	
	public Rectangle(double l, double w, String n)
	{
		length = l;
		width = w;
		name = n;
	}
	
	@Override
	public double getArea() {
		return length * width;
	}

	@Override
	public String getName() {
		return name;
	}
	
}
